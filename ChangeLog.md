# Changelog for voicebase

0.2.0.0 - Nov 1 2018

* Backwards incompatible rewrite
* PCI redaction supported

## Unreleased changes
