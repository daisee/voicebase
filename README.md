# Haskell bindings for voice base api

http://voicebase.readthedocs.io/en/v2-beta/

## Usage

```haskell
    import VoicebaseClient
    import Json.TranscriptTypes

    main = do
      result <- transcribeFile "your bearer token" "./audio.mp3"
      -- get validated response or error
      case result of
        Left err -> print $ err
        Right val -> putStrLn val

      -- get a parsed structure
      parsed <- transcribeParse "your bearer token" "./audio.mp3"
      case parsed of 
        Left err -> print $ err
        Right val -> print $ latestTranscriptsWordsTranscripts . transcriptsLatestTranscripts . mediaTranscripts . topLevelMedia val

```

There is also a main file which shows usage


## Features

+ Post audio file to voicebase
+ Poll for progress
+ Get resulting transcript or valid json or an error.


## Source
The source repo can be found here: https://bitbucket.org/daisee/voicebase
