{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Main where

import           Data.Aeson                 (encode)
import qualified Data.ByteString.Lazy       as LBS
import qualified Data.ByteString.Lazy.Char8 as C8LBS
import           Data.Monoid                ((<>))
import qualified Data.Text                  as Text
import           Network.Mime               (defaultMimeLookup)
import           Options.Applicative
import           System.FilePath            (splitFileName)
import           Voicebase.V2Beta.Client

data Options = Options {
  token :: BearerToken,
  file  :: FilePath
}


sample :: Parser Options
sample = Options
      <$> strOption
          ( long "bearer-token"
         <> metavar "TOKEN"
         <> help "The bearer token obtained from voicebase" )
      <*> strOption
          ( long "file"
         <> metavar "PATH"
         <> help "The file to be posted" )

main :: IO ()
main = do
    Options{..} <- execParser opts
    bytes <- LBS.readFile file
    let mime = defaultMimeLookup $ Text.pack file
    ts <- runVB (withRedaction . defaultConfig token) $
      transcribeAndFetchMedia $ Bytes bytes "audio/wav" file
    print ts
  where
    opts = info (sample <**> helper)
      ( fullDesc
     <> progDesc "Transcribe a file with voicebase"
     <> header "Voicebase transcriber" )

