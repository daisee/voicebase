{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  roundtrip = variant (haskellPackages.callPackage ./roundtrip.nix {});
  roundtrip-aeson = variant (haskellPackages.callPackage ./roundtrip-aeson.nix {
    inherit roundtrip; });
  drv = variant (haskellPackages.callPackage ./default.nix {inherit
  roundtrip-aeson roundtrip; });
in

  if pkgs.lib.inNixShell then drv.env else drv
