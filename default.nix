{ mkDerivation, aeson, async, base, bytestring, filepath, hpack
, HsOpenSSL, hspec, http-client, http-client-openssl, lens
, lens-aeson, mime-types, mtl, optparse-applicative, roundtrip
, roundtrip-aeson, stdenv, text, wreq
}:
mkDerivation {
  pname = "voicebase";
  version = "0.2.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson async base bytestring HsOpenSSL http-client
    http-client-openssl lens lens-aeson mime-types mtl roundtrip
    roundtrip-aeson text wreq
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    aeson base bytestring filepath mime-types optparse-applicative text
  ];
  testHaskellDepends = [ aeson base hspec roundtrip-aeson ];
  preConfigure = "hpack";
  homepage = "https://bitbucket.org/daisee/voicebase";
  description = "Upload audio files to voicebase to get a transcription";
  license = stdenv.lib.licenses.bsd3;
}
